**Description**

Test automation framework for demoqa.com website based on Junit, Selenium, Cucumber and Maven.
Required: the latest chrome or safari browser, java JDK (16), set JAVA_HOME environment variable

**How To RUN IT:**

In terminal:
**mvn clean test**

Using tags (eg. @smokeTest, @inProgress):  
**mvn test -Dcucumber.options="--tags @smoke --tags @inProgress"**

Different browsers:  
**-DbrowserName="safari"  
-DbrowserName="chrome"** 
Chrome is the default browser.

Note: for  safari you must enable the 'Allow Remote Automation' option in Safari's Develop menu


