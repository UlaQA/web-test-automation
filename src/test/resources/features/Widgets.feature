@inProgress
Feature: Widgets page
  As a user I want to use options available on Widgets page.

  Background:
    Given I open Widgets page

  Scenario: Progress bar
    When I select Progress Bar
    And I click Start
    And I click Stop
    Then Blue progress bar is visible

  Scenario: Hover info
    When I select Tool Tips
    And I hover over text
    Then I see "You hovered over the Contrary"

  Scenario: Menu
    When I select Menu
    And I hover over "Main Item 2"
    And I hover over "SUB SUB LIST"
    Then I see "Sub Sub Item 1" and "Sub Sub Item 2"
