Feature: Elements page
  As a user I want to use options available on Elements page.

  Background:
    Given I open Elements page

  Scenario Outline: Text box - positive flow
    When I select Text Box
    And I enter "<name>" in name field
    And I enter "<email>" in email field
    And I enter "<current_address>" in current address field
    And I enter "<permanent_address>" in permanent address field
    And I click Submit
    Then I see all my data: "<name>", "<email>", "<current_address>", "<permanent_address>"

    Examples:
      | name | email       | current_address | permanent_address |
      | Jan  | jan@jan.jan | Szczecin        | Szczecinek        |

  Scenario: Wrong email
    When I select Text Box
    And I enter "test" in email field
    And I click Submit
    Then I see red border of email field

  @inProgress
  Scenario: Web Tables searching
    When I select Web Tables
    And  I enter search phrase "ex" in search field
    Then I see items contain searching phrase "ex"

  Scenario: Pagination
    When I select Web Tables
    And I select "5 rows" per page
    And I add users
      | first name | last name | email          | age | salary | department |
      | Jan1       | Kowalski1 | test1@test.com | 50  | 10000  | Legal      |
      | Jan2       | Kowalski2 | test1@test.com | 50  | 10000  | Legal      |
      | Jan3       | Kowalski3 | test1@test.com | 50  | 10000  | Legal      |
      | Jan4       | Kowalski4 | test1@test.com | 50  | 10000  | Legal      |
      | Jan5       | Kowalski5 | test1@test.com | 50  | 10000  | Legal      |
      | Jan6       | Kowalski6 | test1@test.com | 50  | 10000  | Legal      |
      | Jan7       | Kowalski7 | test1@test.com | 50  | 10000  | Legal      |
      | Jan8       | Kowalski8 | test1@test.com | 50  | 10000  | Legal      |
      | Jan9       | Kowalski9 | test1@test.com | 50  | 10000  | Legal      |
    And I click next page
    Then I see third page of results
  #use api: eg. add 15 users

  @inProgress
  Scenario: Broken image
    When I select Broken Links - Images
    Then I see broken image

  @inProgress
  Scenario: Opening broken link
    When I select Broken Links -Images
    And  I click on broken link
    Then I get page with 500 status code

  @inProgress
  Scenario: Download
    When I select Upload and Download
    And  I click on Download button
    And I confirm I want download file
    Then Download starts
