@inProgress
Feature: Interaction page
  As a user I want to use options available on Interaction page.

  Background:
    Given I open Interactions page

  Scenario: Sortable list
    When I select Sortable
    And I drag and drop elemebnt from the list
    Then Element changes position

  Scenario: Selecting two elements
    When I select Selectable
    And I click on two elements
    Then Elements are blue

  Scenario: Dropping not acceptable element
    When I select Droppable
    And I select Accept
    And I drag and drop Not Accetable element to Drop Here area
    Then Nothing happens

  Scenario: Dropping acceptable element
    When I select Droppable
    And I select Accept
    And I drag and drop Accetable element to Drop Here area
    Then "Dropped!" message appears
