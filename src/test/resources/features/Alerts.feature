Feature: Alerts page
  As a user I want to use options available on Alerts page.

  Background:
    Given I open Alerts page

  Scenario: Opening new tab
    When I select Browser Windows
    And I click New Tab button
    Then New tab "demoqa.com/sample" opens

  @inProgress
  Scenario: Opening window message
    When I select Browser Windows
    And I click New Window Message button
    Then Window message appears

  Scenario: Canceling confirm box
    When I select Alerts
    And I click third Click me button
    And Confirm box appears
    And I select Cancel
    Then Information that alert is closed appears

  Scenario: Entering text in prompt box
    When I select Alerts
    And I click fourth Click me button
    And Prompt box appears
    And I enter 'text' in prompt text field
    And I select OK
    Then I see 'text' on page

  Scenario: Opening large modal
    When I select Modal Dialogs
    And I click Large modal button
    Then Large modal appears
    And Close button is visible