@inProgress
Feature: Forms page
  As a user I want to use options available on Forms page.

  Background:
    Given I open Forms page

  Scenario Outline: Submitting form
    When I select Practice Form
    And I enter my "<first_name>", "<last_name>", "<email>", "<number>", "<subject>" and "current_address>"
    And I select "<sex>"
    And I select "<birth_date>"
    And I select "<hobby>"
    And I select "<state>"
    And I select "<city>"
    Then I see summary window with "<first_name>", "<last_name>", "<email>", "<sex>", "<number>", "<birth_date>", "<subject>", "<hobby>", "<current_address>", "<state>", "<city>"

    Examples:
      | first_name | last_name | email         | sex  | number     | birth_date  | subject | hobby    | current_address | state | city  |
      | Jan        | Kowalski  | test@test.com | Male | 1234567890 | 09 May 1950 | Maths   | sleeping | test            | NCR   | Delhi |

