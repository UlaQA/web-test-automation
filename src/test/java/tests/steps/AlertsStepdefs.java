package tests.steps;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import tests.pages.AlertsPage;

public class AlertsStepdefs {

    AlertsPage alertsPage = new AlertsPage();

    @Given("I open Alerts page")
    public void iOpenAlertsPage() {
        alertsPage.openAlertsPage();

    }

    @When("I select Browser Windows")
    public void iSelectBrowserWindows() {
        alertsPage.clickBrowserWindowsButton();
    }

    @And("I click New Tab button")
    public void iClickNewTabButton() {
        alertsPage.clickNewTabButton();
    }

    @Then("New tab {string} opens")
    public void newTabOpens(String url) throws InterruptedException {
        Assert.assertTrue(alertsPage.isNewTabOpened(url));
    }

    @And("I click New Window Message button")
    public void iClickNewWindowMessageButton() {
        alertsPage.clickNewWindowMessageButton();
    }

    @Then("Window message appears")
    public void windowMessageAppears() {
        Assert.assertTrue(alertsPage.isNewMessageWindowOpen());
    }

    @When("I select Alerts")
    public void iSelectAlerts() {
        alertsPage.clickAlertsButton();
    }

    @And("I click third Click me button")
    public void iClickThirdClickMeButton() {
        alertsPage.clickConfirmBoxButton();
    }

    @And("Confirm box appears")
    public void confirmBoxAppears() {
        alertsPage.waitForAlert();
    }

    @And("I select Cancel")
    public void iSelectCancel() {
        alertsPage.cancelAlert();
    }

    @Then("Information that alert is closed appears")
    public void informationThatAlertIsClosedAppears() {
        Assert.assertTrue(alertsPage.isClosedAlertInformationVisible());
    }

    @And("I click fourth Click me button")
    public void iClickFourthClickMeButton() {
        alertsPage.clickPromptBoxButton();
    }

    @And("Prompt box appears")
    public void promptBoxAppears() {
        alertsPage.waitForAlert();
    }

    @And("I enter {string} in prompt text field")
    public void iEnterTextInPromptTextField(String text) {
        alertsPage.enterTextAlert(text);
    }

    @And("I select OK")
    public void iSelectOK() {
        alertsPage.confirmAlert();
    }

    @Then("I see {string} on page")
    public void iSeeTextOnPage(String text) {
        Assert.assertTrue(alertsPage.isTextAlertVisible(text));
    }

    @When("I select Modal Dialogs")
    public void iSelectModalDialogs() {
        alertsPage.clickModalDialogsButton();
    }

    @And("I click Large modal button")
    public void iClickLargeModalButton() {
        alertsPage.clickLargeModalButton();
    }

    @Then("Large modal appears")
    public void largeModalAppears() {
        Assert.assertTrue(alertsPage.isTextOnModalVisible());
    }

    @And("Close button is visible")
    public void closeButtonIsVisible() {
        Assert.assertTrue(alertsPage.isCloseModalButtonDisplayed());
    }
}
