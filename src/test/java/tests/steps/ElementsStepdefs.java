package tests.steps;


import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import tests.pages.BasePage;
import tests.pages.ElementsPage;

import java.util.concurrent.TimeUnit;

public class ElementsStepdefs {

    BasePage basePage = new BasePage();
    ElementsPage elementsPage = new ElementsPage();

    @Given("I open demoqa main page")
    public void iOpenDemoqaMainPage() {
        basePage.goToMainPage();
    }

    @Given("I open Elements page")
    public void iOpenElementsPage() {
        elementsPage.openElementsPage();
    }

    @When("I select Text Box")
    public void iSelectTextBox() {
        elementsPage.clickTextBoxButton();
    }

    @And("I enter {string} in name field")
    public void iEnterInNameField(String myName) {
        elementsPage.enterName(myName);
    }

    @And("I enter {string} in email field")
    public void iEnterMyEmailAddress(String myEmail) {
        elementsPage.enterEmail(myEmail);
    }

    @And("I enter {string} in current address field")
    public void iEnterMyCurrentAddress(String currentAddress) {
        elementsPage.enterAddress(currentAddress);
    }

    @And("I enter {string} in permanent address field")
    public void iEnterMyPermanentAddress(String permanentAddress) {
        elementsPage.enterPermanentAddress(permanentAddress);
    }

    @And("I click Submit")
    public void iClickSubmit() throws InterruptedException {
        elementsPage.clickSubmitButton();
        TimeUnit.SECONDS.sleep(5);
    }

    @Then("I see all my data: {string}, {string}, {string}, {string}")
    public void iSeeAllMyData(String name, String email, String address1, String address2) {
        Assert.assertEquals("Name:" + name, elementsPage.getName());
        Assert.assertEquals("Email:" + email, elementsPage.getEmail());
        Assert.assertEquals("Current Address :" + address1, elementsPage.getFirstAddress());
        Assert.assertEquals("Permananet Address :" + address2, elementsPage.getSecondAddress());
        // test failed: there is a typo on tested website
    }

    @Then("I see red border of email field")
    public void iSeeRedBorderOfEmailField() {
        System.out.println(elementsPage.getBorderColor());
        Assert.assertTrue(elementsPage.getBorderColor().contains("rgb(255, 0, 0)"));

    }

    @When("I select Web Tables")
    public void iSelectWebTables() {
        elementsPage.clickWebTablesButton();
    }

    @And("I enter search phrase {string} in search field")
    public void iEnterSearchPhraseInSearchField(String searchPhrase) {
        elementsPage.enterSearchPhrase(searchPhrase);
    }

    @Then("I see items contain searching phrase {string}")
    public void iSeeItemsContainSearchingPhrase(String searchingPhrase) {
        Assert.assertTrue(elementsPage.phraseIsFound(searchingPhrase));
    }

    @And("I select {string} per page")
    public void iSelectRowsPerPage(String elementsOnPage) {
        elementsPage.setRowsPerPage(elementsOnPage);
    }

    @And("I add users")
    public void iAddUsers(DataTable users) throws InterruptedException {
        elementsPage.addUsers(users);
    }

    @And("I click next page")
    public void iClickNextPage() throws InterruptedException {
        elementsPage.clickNextButton();
    }

    @Then("I see third page of results")
    public void iSeeSecondPageOfResults() {
        Assert.assertTrue(elementsPage.isTotalPagesCorrect());
    }
}
