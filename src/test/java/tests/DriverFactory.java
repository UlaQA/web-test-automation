package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.safari.SafariDriver;


public class DriverFactory {
    public static WebDriver driver;
    public static ChromeOptions options;
    private static final String DEFAULT_BROWSER = "chrome";

    private static String getBrowserName() {
        String browser = System.getProperty("browserName");
        if (browser == null) {
            browser = DEFAULT_BROWSER;
        }
        return browser;
    }

    public static void setUpDriver() {
        WebDriverManager.chromedriver().setup();
    }

    public void initializeDriver() {
        switch (getBrowserName().toLowerCase()) {
            case "safari" -> {
                driver = new SafariDriver();
                driver.manage().window().maximize();
            }
            case "chrome" -> {
                // Required for Gitlab CI/CD
                options = new ChromeOptions();
                options.addArguments("--no-sandbox");
                options.addArguments("--disable-dev-shm-usage");
                options.addArguments("--headless");
                driver = new ChromeDriver(options);
                driver.manage().window().maximize();
            }
            default -> System.out.println("browser not supported");
        }
    }

    public void destroyDriver() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }
}
