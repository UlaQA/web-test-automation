package tests;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Hooks extends DriverFactory {

    Logger logger = LoggerFactory.getLogger(Hooks.class);

    @Before // run before the first step of each scenario.
    public void beforeScenario(Scenario scenario) {
        logger.info(String.format("(Before) Scenario name: %s", scenario.getName()));
        initializeDriver();
    }

    @After
    public void afterScenario(Scenario scenario) throws IOException {
        logger.info(String.format("(After) Scenario finished with: %s",
                (scenario.isFailed() ? " FAILED" : " SUCCESS")));

        if (scenario.isFailed()) {
            File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
            FileUtils.copyFile(srcFile, new File(currentPath + "/target/screenshots/" + scenario.getName()
                    + "-" + new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(new Date()) + ".png"));
        }

        destroyDriver();
    }

}
