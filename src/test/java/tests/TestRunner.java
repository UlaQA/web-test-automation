package tests;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        tags = "not @inProgress",
        plugin = {
                "pretty",
                "html:target/cucumber-report.html",
                "json:target/cucumber.json"}
)

public class TestRunner extends DriverFactory {

    @BeforeClass
    public static void setup() {
        System.out.println("Setup Chrome Driver");
        setUpDriver();
    }

}