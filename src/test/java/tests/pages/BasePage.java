package tests.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static tests.DriverFactory.driver;

public class BasePage {

    String mainPageUrl = "https://www.demoqa.com";

    public BasePage() {
        PageFactory.initElements(driver, this);
    }

    public void goToMainPage() {
        driver.get(mainPageUrl);
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.titleContains("ToolsQA"));

    }

    public void scrollDown() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,250)");
    }

}