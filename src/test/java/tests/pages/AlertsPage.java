package tests.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

import static tests.DriverFactory.driver;

public class AlertsPage extends BasePage {

    @FindBy(how = How.XPATH, using = "//*[text()='Browser Windows']")
    public WebElement browserWindowsButton;

    @FindBy(how = How.ID, using = "tabButton")
    public WebElement newTabButton;

    @FindBy(how = How.ID, using = "messageWindowButton")
    public WebElement newWindowMessageButton;

    @FindBy(how = How.XPATH, using = "//*[text()='Alerts']")
    public WebElement alertsButton;

    @FindBy(how = How.ID, using = "confirmButton")
    public WebElement confirmBoxButton;

    @FindBy(how = How.ID, using = "confirmResult")
    public WebElement cancelConfirmation;

    @FindBy(how = How.ID, using = "promtButton")
    public WebElement promptBoxButton;

    @FindBy(how = How.ID, using = "promptResult")
    public WebElement promptTextConfirmation;

    @FindBy(how = How.XPATH, using = "//*[text()='Modal Dialogs']")
    public WebElement modalDialogsButton;

    @FindBy(how = How.ID, using = "showLargeModal")
    public WebElement largeModalButton;

    @FindBy(how = How.CLASS_NAME, using = "modal-content")
    public WebElement modalWindow;

    @FindBy(how = How.ID, using = "closeLargeModal")
    public WebElement closeModalButton;

    public void openAlertsPage() {
        driver.get("https://demoqa.com/alertsWindows");
    }

    public void clickBrowserWindowsButton() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", browserWindowsButton);
        browserWindowsButton.click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.urlContains("demoqa.com/browser-windows"));
    }

    public void clickNewTabButton() {
        newTabButton.click();
    }

    public boolean isNewTabOpened(String url) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));

        ArrayList<String> tabs2 = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        String currentUrl = driver.getCurrentUrl();
        return currentUrl.contains(url);
    }

    public void clickNewWindowMessageButton() {
        newWindowMessageButton.click();
    }

    public boolean isNewMessageWindowOpen() {
        for (String windowsHandle : driver.getWindowHandles()) {
            driver.switchTo().window(windowsHandle);
            if (driver.switchTo().activeElement().getText().contains("Knowledge increases by sharing but not by saving.")) {
                return true;
            }
        }
        return false;
    }

    public void clickAlertsButton() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", alertsButton);
        alertsButton.click();
    }

    public void clickConfirmBoxButton() {
        confirmBoxButton.click();
    }

    public void waitForAlert() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.alertIsPresent());
    }

    public void cancelAlert() {
        driver.switchTo().alert().dismiss();
    }

    public void confirmAlert() {
        driver.switchTo().alert().accept();
    }

    public boolean isClosedAlertInformationVisible() {
        return cancelConfirmation.getText().contains("You selected Cancel");
    }

    public void clickPromptBoxButton() {
        promptBoxButton.click();
    }

    public void enterTextAlert(String text) {
        driver.switchTo().alert().sendKeys(text);
    }

    public boolean isTextAlertVisible(String text) {
        return promptTextConfirmation.getText().contains(text);
    }

    public void clickModalDialogsButton() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", modalDialogsButton);
        modalDialogsButton.click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.urlContains("demoqa.com/modal-dialogs"));
    }

    public void clickLargeModalButton() {
        largeModalButton.click();
    }

    public boolean isTextOnModalVisible() {
        return modalWindow.getText().contains("Lorem Ipsum is simply dummy");
    }

    public boolean isCloseModalButtonDisplayed() {
        return closeModalButton.isDisplayed();
    }
}
