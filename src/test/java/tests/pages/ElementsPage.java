package tests.pages;

import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Map;

import static tests.DriverFactory.driver;

public class ElementsPage extends BasePage {

    @FindBy(how = How.XPATH, using = "//*[text()='Text Box']")
    public WebElement textBoxButton;

    @FindBy(how = How.XPATH, using = "//*[text()='Web Tables']")
    public WebElement webTablesButton;

    @FindBy(how = How.ID, using = "userName")
    public WebElement fullNameField;

    @FindBy(how = How.ID, using = "userEmail")
    public WebElement emailField;

    @FindBy(how = How.ID, using = "currentAddress")
    public WebElement currentAddressField;

    @FindBy(how = How.ID, using = "permanentAddress")
    public WebElement permanentAddressField;

    @FindBy(how = How.ID, using = "submit")
    public WebElement submitButton;

    @FindBy(how = How.ID, using = "name")
    public WebElement nameInfo;

    @FindBy(how = How.ID, using = "email")
    public WebElement emailInfo;

    @FindBy(how = How.ID, using = "searchBox")
    public WebElement searchBox;

    @FindBys({@FindBy(how = How.ID, using = "output"), @FindBy(how = How.ID, using = "currentAddress")})
    //two "currentAddress" elements on page
    public WebElement currentAddressInfo;

    @FindBys({@FindBy(how = How.ID, using = "output"), @FindBy(how = How.ID, using = "permanentAddress")})
    public WebElement permanentAddressInfo;

    @FindBy(how = How.CLASS_NAME, using = "rt-tbody")
    public List<WebElement> table;

    @FindBy(how = How.XPATH, using = "//select[@aria-label='rows per page']")
    //    "select-wrap -pageSizeOptions")
    public WebElement pageSizeSelector;

    @FindBy(how = How.ID, using = "addNewRecordButton")
    public WebElement addUserButton;

    @FindBy(how = How.ID, using = "firstName")
    public WebElement firstNameField;

    @FindBy(how = How.ID, using = "lastName")
    public WebElement lastNameField;

    @FindBy(how = How.ID, using = "age")
    public WebElement ageField;

    @FindBy(how = How.ID, using = "salary")
    public WebElement salaryField;

    @FindBy(how = How.ID, using = "department")
    public WebElement departmentField;

    @FindBy(how = How.XPATH, using = "//*[text()='Next']")
    public WebElement nextButton;

    @FindBy(how = How.CLASS_NAME, using = "-totalPages")
    public WebElement totalPagesInfo;

    public void openElementsPage() {
        driver.get("https://demoqa.com/elements");
    }

    public void clickTextBoxButton() {
        textBoxButton.click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.urlContains("demoqa.com/text-box"));
    }

    public void clickWebTablesButton() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", webTablesButton);
        webTablesButton.click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.urlContains("demoqa.com/webtables"));
    }

    public void enterName(String name) {
        fullNameField.sendKeys(name);
    }

    public void enterEmail(String email) {
        emailField.sendKeys(email);
    }

    public void enterAddress(String currentAddress) {
        currentAddressField.sendKeys(currentAddress);
    }

    public void enterPermanentAddress(String permanentAddress) {
        permanentAddressField.sendKeys(permanentAddress);
    }

    public void clickSubmitButton() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", submitButton); //"arguments[0]" means first index of page starting at 0
        submitButton.click();
    }

    public String getName() {
        return nameInfo.getText();
    }

    public String getEmail() {
        return emailInfo.getText();
    }

    public String getFirstAddress() {
        return currentAddressInfo.getAttribute("innerText");
    }

    public String getSecondAddress() {
        return permanentAddressInfo.getText();
    }

    public String getBorderColor() {
        return emailField.getCssValue("border");
    }

    public void enterSearchPhrase(String searchPhrase) {
        searchBox.sendKeys(searchPhrase);
    }

    public boolean phraseIsFound(String searchPhrase) {
        boolean isFound = false;
        for (WebElement row : table) {
            for (WebElement cell : row.findElements(By.className("rt-td"))) {
                bb:
                if (cell.getText().toLowerCase().contains(searchPhrase.toLowerCase())) {
                    isFound = true;
                    break bb;//next row
                }
            }
            if (!isFound) {
                break;
            }
        }
        return isFound;
    }

    public void setRowsPerPage(String pageSize) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", pageSizeSelector);
        Select dropdown = new Select(pageSizeSelector);
        // pageSizeSelector.sendKeys(pageSize);
        dropdown.selectByVisibleText(pageSize);
        dropdown.selectByValue("5");
    }

    public void clickAddUserButton() {
        addUserButton.click();
    }

    public void addUsers(DataTable users) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 5);

        List<Map<String, String>> list = users.asMaps(String.class, String.class);
        for (Map<String, String> stringStringMap : list) {
            wait.until(ExpectedConditions.elementToBeClickable(addUserButton));
            Thread.sleep(2000); //find another way
            clickAddUserButton();
            firstNameField.sendKeys(stringStringMap.get("first name"));
            lastNameField.sendKeys(stringStringMap.get("last name"));
            emailField.sendKeys(stringStringMap.get("email"));
            ageField.sendKeys(stringStringMap.get("age"));
            salaryField.sendKeys(stringStringMap.get("salary"));
            departmentField.sendKeys(stringStringMap.get("department"));
            clickSubmitButton();
            wait.until(ExpectedConditions.elementToBeClickable(addUserButton));
        }
    }

    public void clickNextButton() throws InterruptedException {
        Thread.sleep(2000); //find another way
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", nextButton);
        nextButton.click();
    }

    public boolean isTotalPagesCorrect() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", totalPagesInfo);
        System.out.println(totalPagesInfo.getText());
        return totalPagesInfo.getText().contains("3");
    }


}
